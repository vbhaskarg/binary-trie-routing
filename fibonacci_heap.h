#ifndef FIBONACCI_HEAP_H
#define FIBONACCI_HEAP_H

#include <iostream>
#include <utility>

using namespace std;

class fibonacci_node {
	friend class fibonacci_heap;

	int degree;
	// Distance, Vertex
	pair<long,int> data;
	fibonacci_node *left, *right, *parent ,*child;
	bool child_cut;

public:
	fibonacci_node (void);
	fibonacci_node (pair<long,int> &data);
	~fibonacci_node (void);
};

class fibonacci_heap {
	fibonacci_node *top;

	// Combines two min trees
	fibonacci_node *combine (fibonacci_node *A, fibonacci_node *B);

	// Does a pair-wise combine of the min trees after removing min node
	void pairwise_combine (void);

	// Performs a cacading cut operation
	// Usually done after remove or decrease key
	void cascading_cut (fibonacci_node *curr_node);

	// Combines two fibonacci heaps
	void meld (fibonacci_heap *f_heap);

	// Inserts a min tree into the fibonacci heap
	void insert_min_tree (fibonacci_node *new_node);

	// Removes a min tree from the fibonacci heap
	void remove_min_tree (fibonacci_node *rm_node);

public:
	fibonacci_heap ();
	~fibonacci_heap ();

	// Creates a new node with the provided data and
	// inserts it in the fibonacci heap
	fibonacci_node *insert (pair<long,int> &data);

	// Removes the min node from the fibonacci heap.
	// Returns false if the fibonacci heap is empty, true otherwise
	bool remove_min (pair<long,int> &data);

	// Does an arbitrary remove of the specified node from the fibonacci heap
	// and then performs a cascading cut operation
	void remove (fibonacci_node *rm_node, pair<long,int> &data);

	// Decreases the key value to the new value passed as parameter
	// and then performs a cascading cut operation
	void decrease_key (fibonacci_node *f_node, long new_key);

	// Returns true if the fibonacci heap is empty, false otherwise
	bool is_empty ();
};

#endif
