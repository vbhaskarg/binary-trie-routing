#include <iostream>
#include <cstdlib>
#include <fstream>
#include <climits>
#include <utility>
#include <algorithm>
#include "definitions.h"
#include "dijkstra.h"

using namespace std;

dijkstra :: dijkstra () {
	graph = NULL;
}

dijkstra :: ~dijkstra () {
	delete[] graph;
}

void dijkstra :: create_graph (const char *file_name) {
	int vertexA, vertexB, weight;
	pair<long,int> data;

	ifstream inFile (file_name);
	if (!inFile.is_open()) {
		cout << "\n***Input file doesn't exist!***\n" << endl;
		exit(0);
	}

	inFile >> nOfVertices;
	inFile >> nOfEdges;
	//cout << "No. of vertices: " << nOfVertices << endl;
	//cout << "No. of edges: " << nOfEdges << endl;

	if (graph != NULL)
		delete[] graph;

	graph = new adjacency_list[nOfVertices];

	for (long i = 0; i < nOfEdges; i++) {
		inFile >> vertexA;
		inFile >> vertexB;
		inFile >> weight;
		if (vertexA >= nOfVertices ||
			vertexB >= nOfVertices ||
			weight > MAX_WEIGHT) {
			cout << "\n***Invalid input!***\n" << endl;
			cout << "Either vertices or edge weights are out of bounds" << endl;
			cout << "Vertex A: " << vertexA << endl;
			cout << "Vertex B: " << vertexB << endl;
			cout << "Weight(AB): " << weight << endl;
			exit(0);
		}

		graph[vertexA].neighbors.push_back(vertexB);
		graph[vertexA].weights.push_back(weight);

		graph[vertexB].neighbors.push_back(vertexA);
		graph[vertexB].weights.push_back(weight);
	}

	inFile.close();
}

void dijkstra :: compute (int src_node, int dest_node, bool is_single_dest) {
	pair<long,int> data;
	fibonacci_heap f_heap;

	if (src_node >= nOfVertices || dest_node >= nOfVertices) {
		cout << "Either either source node or destination node is out of bounds" << endl;
		exit(0);
	}

	for (int vertex = 0; vertex < nOfVertices; vertex++) {
		graph[vertex].previous = -1;
		graph[vertex].visited = false;
		graph[vertex].distance = LONG_MAX;
		data = make_pair(LONG_MAX, vertex);
		graph[vertex].my_node = f_heap.insert(data);
	}

	graph[src_node].distance = 0;
	f_heap.decrease_key(graph[src_node].my_node, 0);

	while (!f_heap.is_empty()) {
		f_heap.remove_min(data);
		graph[data.second].visited = true;
		// Check if we found the destination node
		if (is_single_dest && data.second == dest_node)
			break;

		// We have encountered a vertex that hasn't been relaxed yet.
		// Disconnected graph.
		if (data.first == LONG_MAX)
			break;

		for (int i = 0; i < graph[data.second].neighbors.size(); i++) {
			int neighbor = graph[data.second].neighbors[i];
			if (!graph[neighbor].visited) {
				long dist = data.first + graph[data.second].weights[i];
				if (dist < graph[neighbor].distance) {
					graph[neighbor].distance = dist;
					f_heap.decrease_key(graph[neighbor].my_node, dist);
					graph[neighbor].previous = data.second;
				}
			}
		}
	}
}

bool dijkstra :: get_shortest_path (int src_node, int dest_node,
	long &distance, vector<int> &result) {

	int curr_node;
	vector<int> path;

	if (src_node >= nOfVertices || dest_node >= nOfVertices) {
		cout << "Either either source node or destination node is out of bounds" << endl;
		exit(0);
	}

	distance = graph[dest_node].distance;

	curr_node = dest_node;
	path.push_back(curr_node);
	while (curr_node != src_node) {
		curr_node = graph[curr_node].previous;
		if (curr_node == -1)
			return false;
		path.push_back(curr_node);
	}
	reverse(path.begin(), path.end());
	swap(path,result);

	return true;
}
