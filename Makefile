CC = g++-4.8 -std=c++11
DEBUG = -g
CFLAGS = -c $(DEBUG)
LFLAGS = $(DEBUG)

SRC_DIR = .
BIN_DIR = .
OBJ_DIR = .


all: routing ssp

routing: fibonacci_heap.o dijkstra.o binary_trie.o routing.o
	$(CC) $(LFLAGS) -o $(BIN_DIR)/routing \
		$(OBJ_DIR)/fibonacci_heap.o \
		$(OBJ_DIR)/dijkstra.o \
		$(OBJ_DIR)/binary_trie.o \
		$(OBJ_DIR)/routing.o

ssp: fibonacci_heap.o dijkstra.o ssp.o
	$(CC) $(LFLAGS) -o $(BIN_DIR)/ssp \
		$(OBJ_DIR)/fibonacci_heap.o \
		$(OBJ_DIR)/dijkstra.o \
		$(OBJ_DIR)/ssp.o

routing.o: $(SRC_DIR)/routing.cpp
	$(CC) $(CFLAGS) -o $(OBJ_DIR)/routing.o $(SRC_DIR)/routing.cpp

ssp.o: $(SRC_DIR)/ssp.cpp
	$(CC) $(CFLAGS) -o $(OBJ_DIR)/ssp.o $(SRC_DIR)/ssp.cpp

dijkstra.o: $(SRC_DIR)/dijkstra.cpp
	$(CC) $(CFLAGS) -o $(OBJ_DIR)/dijkstra.o $(SRC_DIR)/dijkstra.cpp

binary_trie.o: $(SRC_DIR)/binary_trie.cpp
	$(CC) $(CFLAGS) -o $(OBJ_DIR)/binary_trie.o $(SRC_DIR)/binary_trie.cpp

fibonacci_heap.o: $(SRC_DIR)/fibonacci_heap.cpp
	$(CC) $(CFLAGS) -o $(OBJ_DIR)/fibonacci_heap.o $(SRC_DIR)/fibonacci_heap.cpp

clean: 
	rm -f *.o
	rm -f ssp
	rm -f routing
