#include <iostream>
#include <cstdlib>
#include <fstream>
#include <climits>
#include <vector>
#include <utility>
#include <algorithm>
#include "definitions.h"
#include "fibonacci_heap.h"
#include "dijkstra.h"

using namespace std;

int main(int argc, char *argv[]) {
	char *file_name = NULL;
	int src_node, dest_node;
	vector<int> path;
	long distance;
	dijkstra ssp;

	if (argc != 4) {
		cout << "\n***Invalid usage!***\n" << endl;
		cout << "Usage:" << endl;
		cout << "\t./ssp file_name source_node destination_node" << endl << endl;
		exit(0);
	}

	file_name = argv[1];
	src_node = atoi(argv[2]);
	dest_node = atoi(argv[3]);

	ssp.create_graph(file_name);
	ssp.compute(src_node, dest_node, true);
	if (!ssp.get_shortest_path(src_node, dest_node, distance, path)) {
		// cout << "\n***No path exists from " << src_node << " to " << dest_node << "!***\n" << endl;
		return 0;
	}

	cout << distance << endl;
	for (int i = 0; i < path.size(); i++) {
		cout << path[i] << " ";
	}
	cout << endl;

    return 0;
}
