#ifndef BINARY_TRIE_H
#define BINARY_TRIE_H

#include <iostream>
#include <utility>

using namespace std;

class trie_node {
	friend class binary_trie;

	pair<unsigned int,int> data;
	trie_node *child[2];
	int bit_num;
	bool is_element;

public:
	trie_node (int bit_num);
	trie_node (pair<unsigned int,int> &data);
	~trie_node (void);
};

class binary_trie {
	trie_node *root;

	// Does a post order traversal and removes subtries
	// in which the next hop is the same for all destinations.
	bool post_order(trie_node *curr_node, pair<unsigned int,int> &data);

public:
	binary_trie ();
	~binary_trie ();

	// Inserts the pair <IP address of Destination, next-hop router>
	// in the binary trie
	void insert (pair<unsigned int,int> &data);

	// Returns the value of the key in the binary trie
	int search (unsigned int key);

	// Calls private function "post_order"
	void do_grouping ();
};

#endif
