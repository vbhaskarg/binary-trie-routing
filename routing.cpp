#include <iostream>
#include <cstdlib>
#include <fstream>
#include <climits>
#include <vector>
#include <utility>
#include <algorithm>
#include <cstring>
#include "definitions.h"
#include "fibonacci_heap.h"
#include "binary_trie.h"
#include "dijkstra.h"

using namespace std;

struct routing_table {
	unsigned int my_ip;
	binary_trie my_table;
};

unsigned int *two_pow;

// A utility function that converts IP address
// from string to unsigned int
unsigned int ip_to_int (char *ip) {
	unsigned int part, int_ip = 0;
    char *part_str;
    int i = 3;

    part_str = strtok(ip, ".");
    while (part_str != NULL && i >= 0) {
        part = atoi(part_str);
        int_ip += part * two_pow[8*i];
        part_str = strtok(NULL, ".");
        i--;
    }

	return int_ip;
}

void create_routing_tables (dijkstra &ssp, routing_table *r_table, int nOfRouters) {
	long distance;
	vector<int> path;
	pair<unsigned int,int> data;

	// Construct routing table for all the routers in the network
	for (int src = 0; src < nOfRouters; src++) {
		// Computing shortest path from src to all nodes in the graph.
		ssp.compute(src, 0, false);

		for (int dest = 0; dest < nOfRouters; dest++) {
			if (src != dest) {
				if (ssp.get_shortest_path(src, dest, distance, path)) {
					data = make_pair(r_table[dest].my_ip, path[1]);
					r_table[src].my_table.insert(data);
				}
			}
		}

		r_table[src].my_table.do_grouping();
	}
}

void start_routing (routing_table *r_table, int src_node, int dest_node) {
	int curr_node = src_node;

	if (curr_node == dest_node)
		return;

	do {
		curr_node = r_table[curr_node].my_table.search(r_table[dest_node].my_ip);
	} while (curr_node != dest_node);
	cout << endl;
}

int main(int argc, char *argv[]) {
	char *graph_file_name, *ip_file_name;
	int src_node, dest_node;
	dijkstra ssp;
	vector<int> path;
	long distance;
	int nOfRouters;
	ifstream inFile;

	if (argc != 5) {
		cout << "\n***Invalid usage!***\n" << endl;
		cout << "Usage:" << endl;
		cout << "\t./ssp graph_file_name ip_addr_file_name source_node destination_node\n\n";
		exit(0);
	}

	graph_file_name = argv[1];
	ip_file_name = argv[2];
	src_node = atoi(argv[3]);
	dest_node = atoi(argv[4]);

	inFile.open(graph_file_name);
	if (!inFile.is_open()) {
		cout << "\n***Input graph file doesn't exist!***\n" << endl;
		exit(0);
	}

	inFile >> nOfRouters;
	inFile.close();

	routing_table *r_table = new routing_table[nOfRouters];

	// Compute pow(2,0) to pow(2,31)
	// Required for converting IP addresses from string to unsigned int
	two_pow = new unsigned int[32];
	for (int i = 0; i < 32; i++)
		two_pow[i] = pow(2,i);

	inFile.open(ip_file_name);
	if (!inFile.is_open()) {
		cout << "\n***Input IP addresses file doesn't exist!***\n" << endl;
		exit(0);
	}
	for (int i = 0; i < nOfRouters; i++) {
		char ip[20];
		inFile >> ip;
		r_table[i].my_ip = ip_to_int(ip);
		//cout << r_table[i].my_ip << endl;
	}
	inFile.close();

	delete[] two_pow;

	ssp.create_graph(graph_file_name);

	create_routing_tables (ssp, r_table, nOfRouters);

	// Compute the shortest path from source to destination
	ssp.compute(src_node, dest_node, true);
	if (!ssp.get_shortest_path(src_node, dest_node, distance, path)) {
		// cout << "\n***No path exists from " << src_node << " to " << dest_node << "!***\n" << endl;
		return 0;
	}

	cout << distance << endl;
	start_routing(r_table, src_node, dest_node);

	delete[] r_table;

    return 0;
}
