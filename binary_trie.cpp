#include <climits>
#include <algorithm>
#include <queue>
#include <bitset>
#include "binary_trie.h"

int msb_int(unsigned int x) {
	int ret = sizeof(unsigned int) * CHAR_BIT - 1;
	return x ? ret - __builtin_clz(x) : ret;
}

trie_node :: trie_node (int bit_num) {
	is_element = false;
	child[0] = NULL;
	child[1] = NULL;
	this->bit_num = bit_num;
}

trie_node :: trie_node (pair<unsigned int,int> &data) {
	is_element = true;
	child[0] = NULL;
	child[1] = NULL;
	swap(this->data, data);
}

trie_node :: ~trie_node (void) {
	delete child[0];
	delete child[1];
}

binary_trie :: binary_trie () {
	root = NULL;
}

binary_trie :: ~binary_trie () {
	delete root;
}

void binary_trie :: insert (pair<unsigned int,int> &data) {
	trie_node *new_node = new trie_node(data);
	trie_node *curr_node, *parent_node, *branch_node;
	int first_diff;
	vector<trie_node *> path;

	if (root == NULL) {
		root = new_node;
		return;
	}

	curr_node = root;

	// Traverse down the trie to an element node
	while (!curr_node->is_element) {
		path.push_back(curr_node);
		curr_node = curr_node->child[((new_node->data.first >> (31-curr_node->bit_num))&1)];
	}

	// Check if the key is matching the element node
	if (curr_node->data.first == new_node->data.first) {
		curr_node->data.second = new_node->data.second;
		delete new_node;
		return;
	}

	// Identify the first bit where the key of element node and new key differ
	first_diff = 31-msb_int(curr_node->data.first^new_node->data.first);

	parent_node = NULL;

	// Identify the position of the new branch node on the path taken
	for (int i = 0; i < path.size(); i++) {
		if(path[i]->bit_num > first_diff)
			break;
		parent_node = path[i];
	}

	// Identify if the new element node is going to be a left child
	// or a right child of the new branch node
	int child_num = (new_node->data.first >> (31-first_diff))&1;

	// Create new branch node
	branch_node = new trie_node(first_diff);
	branch_node->child[child_num] = new_node;

	// Insert the new branch node
	if (parent_node == NULL) {
		branch_node->child[1-child_num] = root;
		root = branch_node;
	} else {
		int temp = (new_node->data.first >> (31-parent_node->bit_num))&1;
		branch_node->child[1-child_num] = parent_node->child[temp];
		parent_node->child[temp] = branch_node;
	}
}

int binary_trie :: search (unsigned int key) {
	bitset<32> binary_key(key);
	trie_node *curr_node, *parent_node = NULL;

	if (root == NULL)
		return -1;

	curr_node = root;

	// Traverse down the trie to an element node
	while (!curr_node->is_element) {
		parent_node = curr_node;
		curr_node = curr_node->child[((key >> (31-curr_node->bit_num))&1)];
	}

	if (parent_node == NULL)
		return -1;

	cout << binary_key.to_string().substr(0, parent_node->bit_num+1) << " ";

	return curr_node->data.second;
}

bool binary_trie :: post_order(trie_node *curr_node, pair<unsigned int,int> &data) {
	bool left_result, right_result;
	pair<unsigned int,int> left_data, right_data;

	if (curr_node == NULL)
		return false;
	
	if (curr_node->is_element) {
		data = curr_node->data;
		return true;
	}

	left_result = post_order(curr_node->child[0], left_data);
	right_result = post_order(curr_node->child[1], right_data);

	// Remove subtries in which the next hop is the same
	if (left_result && right_result && (left_data.second == right_data.second)) {
		curr_node->is_element = true;
		data.first = curr_node->data.first = left_data.first;
		data.second = curr_node->data.second = left_data.second;

		delete curr_node->child[0];
		delete curr_node->child[1];

		curr_node->child[0] = curr_node->child[1] = NULL;

		return true;
	}

	return false;
}

void binary_trie :: do_grouping () {
	pair<unsigned int,int> data;

	post_order(root, data);
}
