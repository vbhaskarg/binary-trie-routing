#ifndef DIJKSTRA_H
#define DIJKSTRA_H

#include <vector>
#include "fibonacci_heap.h"

using namespace std;

struct adjacency_list {
	fibonacci_node *my_node;
	vector<int> neighbors;
	vector<int> weights;
	int previous = -1;
	bool visited = false;
	long distance = LONG_MAX;
};

class dijkstra {
private:
	int nOfVertices;
	long nOfEdges;
	adjacency_list *graph;

public:
	dijkstra ();
	~dijkstra ();

	// Takes input from a file and creates an adjacency list
	// for the vertices in the graph
	void create_graph (const char *file_name);

	// Uses Dijkstra's Single Source Shortest Path algorithm to compute
	// shortest path between source and destination. Should be called only
	// after running "create_graph". "is_single_dest" specifies whether the
	// program exectuion should stop after encountering the destination node
	void compute (int src_node, int dest_node, bool is_single_dest);

	// Writes the shortest distance and the path between source and destination
	// nodes in the parameters passed. Returns false if there no such path, and
	// true otherwise.
	bool get_shortest_path (int src_node, int dest_node,
		long &distance, vector<int> &result);
};

#endif
