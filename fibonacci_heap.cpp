#include <unordered_map>
#include "fibonacci_heap.h"

fibonacci_node :: fibonacci_node (void) {
	degree = 0;
	data.first = 0;
	data.second = 0;
	left = this;
	right = this;
	parent = NULL;
	child = NULL;
	child_cut = false;
}

fibonacci_node :: fibonacci_node (pair<long,int> &data) {
	degree = 0;
	left = this;
	right = this;
	parent = NULL;
	child = NULL;
	swap(this->data, data);
}

fibonacci_node :: ~fibonacci_node (void) {
	
}


fibonacci_heap :: fibonacci_heap () {
	top = NULL;
}

fibonacci_heap :: ~fibonacci_heap () {

}

fibonacci_node *fibonacci_heap :: combine (fibonacci_node *A, fibonacci_node *B) {
	if (A == NULL)
		return B;

	if (B == NULL)
		return A;

	if (B->data.first < A->data.first)
		swap(A,B);

	B->parent = A;
	if (A->child != NULL) {
		fibonacci_node *adj_node = A->child->left;
		A->child->left = B;
		B->right = A->child;
		adj_node->right = B;
		B->left = adj_node;
	} else {
		A->child = B;
	}
	B->child_cut = false;
	A->degree++;

	return A;
}

void fibonacci_heap :: pairwise_combine (void) {
	unordered_map<int, fibonacci_node *> degree_map;
	fibonacci_node *curr_node, *next_node, *new_node;

	if (top == NULL)
		return;

	curr_node = top;
	do {
		next_node = curr_node->right;
		curr_node->left = curr_node->right = curr_node;
		curr_node->parent = NULL;

		while (degree_map.find(curr_node->degree) != degree_map.end()) {
			int curr_degree = curr_node->degree;
			curr_node = combine(degree_map.at(curr_degree), curr_node);
			degree_map.erase(curr_degree);
		}

		pair<int,fibonacci_node *> new_entry (curr_node->degree, curr_node);
		degree_map.insert(new_entry);
		curr_node = next_node;
	} while (curr_node != top);

	top = NULL;

	for (auto& itr: degree_map)
		insert_min_tree(itr.second);
}

void fibonacci_heap :: cascading_cut (fibonacci_node *curr_node) {
	if (curr_node == NULL)
		return;

	while (curr_node->parent != NULL && curr_node->child_cut == true) {
		fibonacci_node *next_node = curr_node->parent;
		remove_min_tree(curr_node);
		curr_node->child_cut = false;
		insert_min_tree(curr_node);
		curr_node = next_node;
	}

	if (curr_node->parent != NULL)
		curr_node->child_cut = true;
}

void fibonacci_heap :: meld (fibonacci_heap *f_heap) {

	if (f_heap->top == NULL)
		return;

	if (this->top == NULL) {
		top = f_heap->top;
		return;
	}

	fibonacci_node *my_adj_node = top->left;
	fibonacci_node *other_adj_node = f_heap->top->left;

	top->left = other_adj_node;
	other_adj_node->right = top;
	f_heap->top->left = my_adj_node;
	my_adj_node->right = f_heap->top;

	// Update min ptr if necessary
	if (f_heap->top->data.first < top->data.first)
		top = f_heap->top;
}

void fibonacci_heap :: insert_min_tree (fibonacci_node *new_node) {
	fibonacci_heap new_heap;
	new_heap.top = new_node;
	meld (&new_heap);
}

void fibonacci_heap :: remove_min_tree (fibonacci_node *rm_node) {
	fibonacci_node *curr_parent;

	if (rm_node == NULL)
		return;

	rm_node->left->right = rm_node->right;
	rm_node->right->left = rm_node->left;
	curr_parent = rm_node->parent;
	if (curr_parent != NULL) {
		if (curr_parent->degree == 1)
			curr_parent->child = NULL;
		else if (curr_parent->child == rm_node)
			curr_parent->child = rm_node->right;
		curr_parent->degree--;
	}
	rm_node->left = rm_node->right = rm_node;
	rm_node->parent = NULL;
}

fibonacci_node *fibonacci_heap :: insert (pair<long,int> &data) {
	fibonacci_node *new_node = new fibonacci_node(data);
	insert_min_tree(new_node);
	return new_node;
}

bool fibonacci_heap :: remove_min (pair<long,int> &data) {
	fibonacci_node *min = top;
	fibonacci_node *curr_node;

	if (top == NULL)
		return false;

	if (top->right == top)
		top = NULL;
	else
		top = top->right;

	remove_min_tree(min);
	if (min->child != NULL) {
		curr_node = min->child;
		do {
			curr_node->parent = NULL;
			curr_node->child_cut = false;
			curr_node = curr_node->right;
		} while(curr_node != min->child);

		insert_min_tree(min->child);
	}

	pairwise_combine();

	swap(min->data, data);
	delete min;

	return true;
}

void fibonacci_heap :: remove (fibonacci_node *rm_node, pair<long,int> &data) {
	fibonacci_node *curr_node, *min_node, *curr_child, *curr_parent;
	fibonacci_heap new_heap;

	if (rm_node == NULL)
		return;

	if (rm_node == top) {
		remove_min(data);
		return;
	}

	curr_parent = rm_node->parent;
	curr_child = rm_node->child;
	rm_node->child = NULL;
	remove_min_tree(rm_node);
	swap(rm_node->data, data);
	delete rm_node;

	if (curr_child != NULL) {
		min_node = curr_node = curr_child;

		do {
			curr_node->parent = NULL;
			curr_node->child_cut = false;
			if (curr_node->data.first < min_node->data.first)
				min_node = curr_node;
			curr_node = curr_node->right;
		} while(curr_node != curr_child);
	}

	new_heap.top = min_node;
	meld(&new_heap);

	cascading_cut(curr_parent);
}

void fibonacci_heap :: decrease_key (fibonacci_node *f_node, long new_key) {
	if (f_node == NULL)
		return;

	if (f_node->data.first <= new_key)
		return;

	f_node->data.first = new_key;
	if (f_node->parent == NULL) {
		if (new_key < top->data.first)
			top = f_node;
	} else {
		if (new_key < f_node->parent->data.first) {
			fibonacci_node *curr_parent = f_node->parent;
			remove_min_tree(f_node);
			insert_min_tree(f_node);
			cascading_cut(curr_parent);
		}
	}
}

bool fibonacci_heap :: is_empty () {
	return top == NULL ? true : false;
}
